MAKEFLAGS += --warn-undefined-variables
SHELL := bash
.SHELLFLAGS := -eu -o pipefail -c
.DEFAULT_GOAL := default
.DELETE_ON_ERROR:
.SUFFIXES:

ifeq ($(util_dir),)
util_dir := ../
endif

define updatekvfile
  file="$(1)"; \
  key="$(2)"; \
  prompt="$(3)"; \
  touch "$$file" && \
  default=$$(grep "^$$key:" "$$file" | cut -d ":" -f 2- || :); \
  read -p "$$prompt [$$default]: " value; \
  if [ "" == "$$value" ]; then \
    value=$$default; \
  fi; \
  sed -e "s/^$$key.*//; /^$$/d" "$$file" -i; \
  echo "$$key:$$value" >> "$$file"
endef

define ensureusableymlfile
  if [ ! -e "$(1)" ]; then \
    echo "date: $$(date)" > "$(1)"; \
  fi
endef

define updateymlfile
  yq merge --inplace --overwrite "$(1)" "$(1)".tmp; \
  rm -f "$(1)".tmp
endef

define updateymlfiledefault
  file="$(1)"; \
  key="$(2)"; \
  default="$(3)"; \
  $(call ensureusableymlfile,$$file); \
  $(call ensureusableymlfile,$$file.tmp); \
  yq write --inplace "$$file".tmp "$$key" "$$default"
endef

define updateymlfileprompt
  file="$(1)"; \
  key="$(2)"; \
  prompt="$(3)"; \
  $(call ensureusableymlfile,$$file); \
  $(call ensureusableymlfile,$$file.tmp); \
  default=$$(yq read $$file $$key); \
  read -p "$$prompt [$$default]: " value; \
  if [ "" == "$$value" ]; then \
    value=$$default; \
  fi; \
  yq write --inplace "$$file".tmp "$$key" "$$value"
endef

.PHONY: all
all:

.PHONY: default
default: list

.PHONY: list
list: make-list-horizontal

# https://stackoverflow.com/questions/4219255/how-do-you-get-the-list-of-targets-in-a-makefile
.PHONY: make-list-hor make-list-horizontal
make-list-hor: make-list-horizontal
make-list-horizontal:
	@$(MAKE) -pRrni -f $(MAKEFILE_LIST) .dummy 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | sort | egrep -v -e '^[^[:alnum:]]' | xargs

# NodeJs Makefiles
.PHONY: make-list-ver make-list-vertical
make-list-ver: make-list-vertical
make-list-vertical:
	@$(MAKE) -pRrni -f $(MAKEFILE_LIST) .dummy 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | command grep -v -e '^[^[:alnum:]]' | sort

.PHONY: .dummy
.dummy:
