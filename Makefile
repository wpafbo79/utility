MAKEFLAGS += --warn-undefined-variables
SHELL := bash
.SHELLFLAGS := -eu -o pipefail -c
.DEFAULT_GOAL := default
.DELETE_ON_ERROR:
.SUFFIXES:

CRITICAL_DOMAINS = \
		   Git \
		   Make
RECOMMENDED_DOMAINS = \
		      temporary
OTHER_DOMAINS = \
		Bash \
		Jekyll \
		Python \
		Ruby

ifeq ("$(wildcard $(lastword $(MAKEFILE_LIST)))","Makefile")
$(info Run from utility root directory.  Including all domains.)
USE_DOMAINS = *
endif

# Limit included domains here (for testing purposes only).
#USE_DOMAINS =

ifndef USE_DOMAINS
$(info Select domains from the following:)
$(info - Critical domains:  $(CRITICAL_DOMAINS))
$(info - Recommended domains:  $(RECOMMENDED_DOMAINS))
$(info - Other domains:  $(OTHER_DOMAINS))
$(info )
$(info Example:)
$(info USE_DOMAINS = Git Make Bash)
$(info or:)
$(info USE_DOMAINS = *)
$(info )
$(error USE_DOMAINS not defined)
endif

ifeq ($(domains),)
ifeq ($(USE_DOMAINS),*)
domains := $(shell find $(util_dir) -maxdepth 1 -path  '.' -o -path '*/.git' \
	     -prune -o -type d -print | cut -d '/' -f 2)
else
domains := $(USE_DOMAINS)
endif
endif

ifeq ($(util_dir),)
ifneq ($(UTIL_DIR),)
util_dir := $(UTIL_DIR)
else
util_dir := ./
endif
endif

domain_makefiles := $(foreach d, $(domains), $(util_dir)$d/Makefile)
domain_makefiles := $(shell ls $(domain_makefiles) 2>/dev/null | sort -u)

$(shell rm -f $(util_dir)*/.use)
$(foreach d, $(domains), $(shell touch $(util_dir)$d/.use))

.PHONY: all
all:

.PHONY: default
default:

.PHONY: clean
clean: cleanuse

.PHONY: cleanuse
cleanuse:
	@find -name .use -exec rm -v {} \;

-include $(domain_makefiles)
