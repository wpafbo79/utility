#!/bin/bash

set -e -u
set -o pipefail

function find_git_root() {(
  while [ ! -d .git ]; do
    cd ..
    [ "/" == "$(pwd)" ] && echo "No git directory found." && return 1
  done
)}

function fix_new_line() {(
  local dir=${1-.}

  local -a deleted
  local f
  local file
  local filedeleted=false

  cd "$dir"

  readarray -t deleted < <(git status | \
    grep deleted | \
    cut -d ":" -f 2- | \
    sed -e 's/^[ \t]*//'
  )

  git ls-files -z |
    while IFS= read -rd '' f; do
      filedeleted=false
      for file in ${deleted[@]}; do
        if [ "$f" == "$file" ]; then
          echo -e "\r\033[KSkipping deleted file: $f"
          filedeleted=true
        fi
      done
      $filedeleted && continue

      [ $(file --mime-encoding "$f" | grep -v binary | wc -l) -gt 0 ] &&
        (
          echo -ne "\r\033[KChecking for new line at EOF: $f" &&
            tail -n1 < "$f" | read -r _ || echo > "$f"
          echo -ne "\r\033[KConverting new lines: $f" &&
            dos2unix "$f" >/dev/null 2>&1
        ) || (
          echo -e "\r\033[KSkipping binary file: $f"
        )
    done
  echo -en "\r\033[K"
)}

function fix_perms() {(
  local utildir=$(realpath $1)/
  local rootdir=${2-.}

  local -a deleted
  local f
  local file
  local filedeleted=false

  cd "$rootdir"

  local -a extensions=( $(
    for d in "$utildir"/*; do
      [ -e "$d/.use" ] && cat "$d"/functions.git.fix_perms 2>/dev/null
    done
  ) )
  if [ ${#extensions[@]} -gt 0 ]; then
    read -ra extensions < <(
      if [ ${#extensions[@]} -gt 1 ]; then
        printf "! -name \\*%s -a " "${extensions[@]:0:$((${#extensions[@]} - 1))}"
      fi
      echo "! -name \\*${extensions[@]: -1}"
    )
  else
    extensions=( "! -name /dev/null" )
  fi

  readarray -t deleted < <(git status | \
    grep deleted | \
    cut -d ":" -f 2- | \
    sed -e 's/^[ \t]*//'
  )

  git ls-files -z |
    while IFS= read -rd '' f; do
      filedeleted=false
      for file in ${deleted[@]}; do
        if [ "$f" == "$file" ]; then
          echo -e "\r\033[KSkipping deleted file: $f"
          filedeleted=true
        fi
      done
      $filedeleted && continue

      if [ $(git submodule status $f | grep $f | wc -l) -eq 0 ]; then
        eval "find '$f' -type f -perm /111 \( ${extensions[@]} \) -exec chmod -v -x {} \;"
      else
        echo "Skipping submodule: $f"
      fi
    done
)}

function prep_repo() {
  local utildir=$1
  local gitroot=$(find_git_root)
  fix_new_line "$gitroot"
  fix_perms "$utildir" "$gitroot"
}
