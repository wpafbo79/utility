#!/usr/bin/perl

use strict;
use warnings;

my $first=1;
my %ignores;
my $section;
foreach my $line (<STDIN>) {
  next if($line =~ /^$/);
  if($line =~ /^#/) {
    $section=$line;
  }
  else {
    $ignores{$section}{$line} = '';
  }
}
foreach my $k1 (sort keys %ignores) {
  if($first) {
    $first=0;
  }
  else {
    print "\n";
  }
  print $k1;
  foreach my $k2 (sort keys %{ $ignores{$k1} }) {
    print $k2;
  }
}
